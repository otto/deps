# IkiWiki::Setup::Yaml - YAML formatted setup file
#
# Setup file for ikiwiki.
# 
# Passing this to ikiwiki --setup will make ikiwiki generate
# wrappers and build the wiki.
# 
# Remember to re-run ikiwiki --setup any time you edit this file.
#
# name of the wiki
wikiname: Debian Enhancement Proposals
# contact email for wiki
adminemail: plessy@debian.org
# users who are wiki admins
adminuser: []
# users who are banned from the wiki
banned_users: []
# where the source of the wiki is located
srcdir: ./web
# where to build the wiki
destdir: ./out
# base url to the wiki
url: https://dep-team.pages.debian.net/
# url to the ikiwiki.cgi
#cgiurl: http://dep.debian.net/cgi-bin/ikiwiki.cgi
# filename of cgi wrapper to generate
# cgi_wrapper: /srv/alioth.debian.org/chroot/home/groups/dep/cgi-bin/ikiwiki.cgi
# mode for cgi_wrapper (can safely be made suid)
# cgi_wrappermode: 06755
# rcs backend to use
# rcs: 
# plugins to add to the default configuration
add_plugins:
- tag
- toc
- meta
- table
- typography
- pagetemplate
- rawhtml
- img
# plugins to disable
disable_plugins:
- editpage
- openid
- htmlscrubber
- passwordauth
# additional directory to search for template files
templatedir: ./web/.templates
# base wiki source location
underlaydir: /dev/null
# display verbose messages?
#verbose: 1
# log to syslog?
#syslog: 1
# create output files named page/index.html?
usedirs: 1
# use '!'-prefixed preprocessor directives?
prefix_directives: 1
# use page/index.mdwn source files
indexpages: 0
# enable Discussion pages?
discussion: 0
# name of Discussion pages
discussionpage: Discussion
# generate HTML5?
html5: 1
# only send cookies over SSL connections?
sslcookie: 0
# extension to use for new pages
default_pageext: mdwn
# extension to use for html files
htmlext: html
# strftime format string to display date
timeformat: '%a, %d %b %Y %H:%M:%S %z'
# UTF-8 locale to use
locale: en_US.UTF-8
# put user pages below specified page
userdir: ''
# how many backlinks to show before hiding excess (0 to show all)
numbacklinks: 10
# attempt to hardlink source files? (optimisation for large files)
hardlink: 0
# force ikiwiki to use a particular umask (keywords public, group or private, or a number)
umask: 2
# group for wrappers to run in
#wrappergroup: ikiwiki
# extra library and plugin directory
libdir: ''
# environment variables
ENV: {}
# time zone name
#timezone: US/Eastern
# regexp of normally excluded files to include
#include: ^\.htaccess$
# regexp of files that should be skipped
#exclude: ^(*\.private|Makefile)$
# specifies the characters that are allowed in source filenames
wiki_file_chars: -[:alnum:]+/.:_
# allow symlinks in the path leading to the srcdir (potentially insecure)
allow_symlinks_before_srcdir: 0

######################################################################
# core plugins
#   (editpage, htmlscrubber, inline, link, meta, parentlinks, svn)
######################################################################

# htmlscrubber plugin
# PageSpec specifying pages not to scrub
#htmlscrubber_skip: '!*/Discussion'

# inline plugin
# enable rss feeds by default?
rss: 1
# enable atom feeds by default?
atom: 0
# allow rss feeds to be used?
#allowrss: 0
# allow atom feeds to be used?
#allowatom: 0
# urls to ping (using XML-RPC) on feed update
#pingurl: []
